#!/bin/sh
if [ "$1" == "buildall" ]
then
	cd graphs
	./rebuild-graphs.sh
	cd ..
fi

pdflatex main.tex
bibtex main.aux 
pdflatex main.tex
pdflatex main.tex
