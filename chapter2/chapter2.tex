\documentclass[main.tex]{subfiles}
\begin{document}
	\chapter{Una simulazione in esame}
		\section{tmLQCD}
			Un software utilizzato nel campo della QCD su reticolo \`e \textbf{tmLQCD} \cite{tmLQCD-homepage}. 
			
			Questo programma fornisce una implementazione di un algoritmo Polynomial Hybrid
			Monte Carlo per l'evoluzione del sistema fisico e dei programmi supplementari per 
			l'elaborazione dei dati ottenuti nei vari run. 
			
			\`E pensato per funzionare su diverse architetture, in particolare supporta le 
			famiglie di supercomputer IBM Blue Gene, Blue Gene Q, Aurora, tutti i cluster basati 
			su processori della famiglia Intel e supporta parzialmente il calcolo su schede
			grafiche (GPU).

		\section{Motivazioni delle modifiche}
			Il calcolo su GPU \`e abbastanza recente nel panorama del mondo High Performance Computing:
			introdotto da Nvidia con la famiglia di schede GeForce 8, forniscono quello che \`e 
			essenzialmente un coprocessore vettoriale con una ridotta capacit\`a di memoria ma 
			una velocit\`a di calcolo in doppia precisione molto elevata, grazie ad una architettura 
			massicciamente parallela.
			
			Sebbene il supporto in ambito scientifico di questa nuova classe di coprocessori matematici 
			sia di primaria importanza per arrivare ad affrontare problemi di complessit\`a 
			crescente, ci sono tante altre possibili direzioni per adattare il codice 
			tmLQCD a sfruttare in modo ottimale diverse architetture hardware. In particolare 
			le ultime generazioni di processori Intel fascia server possiedono il supporto 
			a una nuova tecnologia, denominata Advanced Vector Extension (\emph{AVX}, in breve) 
			che estende il set d'istruzioni SSE3. 
			
			Questa scelta non fornisce la stessa rapidit\`a di sviluppo in termini prestazionali assicurato
			dal calcolo su GPU, ma il guadagno risiede nell'avere un paradigma di programmazione pi\`u 
			stabile su un periodo di tempo pi\`u lungo. 
			
			Per far capire quale sia l'impatto derivante dall'utilizzo di queste tecnologie, 
			mostriamo di seguito il confronto della velocit\`a d'esecuzione dell'operatore di 
			Dirac con e senza accelerazione hardware:
			
				\begin{center}
					\includegraphics{graphs/graph-serial-vs-slowest-speed}
				\end{center}			
		
			L'asse delle y esprime il numero di operazioni in virgola mobile per secondo,
			mentre sull'asse delle ascisse abbiamo il numero di punti del reticolo,
			espressa la notazione $T \cdot L^3$, dove $T$ \`e l'estensione del 
			reticolo lungo la dimensione temporale, mentre $L$ \`e l'estensione lungo 
			le altre dimensioni spaziali. 
			
			In questo lavoro cerchiamo di esplorare la possibilit\`a di utilizzare la tecnologia 
			AVX.
			
		\section{Le tecnologie in esame}
			L'idea che sta alla base delle modifiche \`e la conversione di un codice che 
			utilizza le vecchie istruzioni SSE3, in una versione equivalente
			che sfrutta la tecnologia AVX, introdotta precedentemente e presentata brevemente
			di seguito:
		
			\subsection{Advanced Vector Extension}
				L'implementazione attuale della matrice di Dirac \`e basata sulle istruzioni SSE, 
				ossia un sottoinsieme di istruzioni dei processori Intel/AMD creato per accelerare
				il calcolo in singola e doppia precisione. Nella pratica, esse forniscono delle 
				istruzioni che permettono ai processori di funzionare, seppur limitatamente, 
				come dei processori vettoriali.
		
				Questa tecnologia ha subito miglioramenti e aggiunte nel corso degli anni ed ha 
				raggiunto la quarta revisione (SSE4), ma presenta alcuni limitazioni dovute 
				all'hardware per cui erano pensate (in particolare, sfruttano un numero limitato
				di registri).
		
				Intel ha risolto questi problemi introducendo una nuova tecnologia, denominata
				\emph{AVX} che affronta questo problema introducendo una nuova classe di primitive 
				per il calcolo parallelo, pur rimanendo retrocompatibile con le vecchie istruzioni SSE.

				I vantaggi, rispetto a quest'ultime, sono molteplici:
				
					\begin{center}
						\begin{itemize}
							\item{registri a 256 bits}
							\item{tutte le istruzioni SSE che lavoravano su registri a 128bit hanno una controparte che lavora sui nuovi registri}
							\item{nuove istruzioni per manipolare e incrociare i dati all'interno dei registri}
							\item{2 porte per le operazioni di load da memoria}
						\end{itemize}
					\end{center}

				Inoltre, questa nuova tecnologia assicura per design che eventuali miglioramenti ed estensioni 
				possano essere integrate con il minimo sforzo nel codice esistente.
				
				Ma, essendo una tecnologia estremamente giovane (\`e stata introdotta con i processori 
				``Sandy Bridge'' all'inizio del 2011) e l'utilizzo in campo scientifico \`e ancora nelle fasi 
				iniziali. In termini pratici, questo implica una carenza di documentazione e informazioni utili 
				per velocizzare lo sviluppo di codice efficiente. 
				
				Pi\`u ricerche di informazioni hanno evidenziato infatti una ridotta quantit\`a di materiale 
				rilevante al nostro scopo, in particolare per quanto riguarda l'ottimizzazione di algoritmi 
				per la moltiplicazione di matrici di dimensione ridotta. Un lavoro simile a quello presentato 
				qui \`e possibile trovarlo online \cite{Smelyanskiy:2011:HLQ:2063384.2063477}.				
				
				per quanto concerne l'operatore di Dirac (e il calcolo scientifico in geneale), \`e molto 
				interessante la nuova dimensione dei registri, in quanto permette di memorizzare due numeri 
				complessi in doppia precisione in un unico registro. Questo rappresenta un vantaggio 
				soprattutto per le applicazioni in parallelo.
				
			\subsection{Prefetching}
				Lo sviluppo delle architetture dei microprocessori moderni ha portato ad incrementare
				notevolmente le capacit\`a di elaborazione delle informazioni, al punto tale da superare 
				di gran lunga la velocit\`a di accesso alla memoria centrale. 
				
				Questo \`e un grosso problema poich\'e, senza le dovute attenzioni, le CPU passerebbero 
				la maggior parte del loro tempo ad attendere i dati dalla RAM senza svolgere alcun lavoro 
				utile. 
				
				Per ovviare a questo problema, nel corso degli anni sono state adottate tecniche che 
				permettono al processore di alleviare questo problema: branch prediction, esecuzione
				fuori ordine e prefetching sono alcune delle tecniche a tutt'oggi in uso. 
				
				In particolare, in riferimento ai calcoli della QCD su reticolo siamo interessati a 
				quest'ultima tecnologia: il prefetching \`e una tecnica che permette al processore 
				di effettuare previsioni su quali dati prelevare dalla memoria \emph{prima} che essi 
				vengano effettivamente richiesti dal codice in esecuzione. 
				
				Nei casi pi\`u semplici, usando	informazioni relative alla \emph{localit\`a spaziale} 
				e alla \emph{localit\`a temporale}, la cpu \`e in grado di effettuare queste previsioni
				in maniera automatica.
			
				Questo meccanismo ha il vantaggio di mantenere il processore occupato, caricando 
				in anticipo i dati necessari ed evitando di dover bloccare l'esecuzione per attendere
				le informazioni dai livelli pi\`u bassi della gerarchia di memoria.

				Ma questo sistema funziona in maniera efficiente solo se l'applicazione organizza
				gli accessi alla RAM seguendo un pattern facilmente prevedibile e se tali informazioni
				sono contigue.
				
				Quest'ultima osservazione \`e la chiave per capire l'importanza di questo sistema: 
				l'implementazione della matrice di Dirac aggiorna tutti i siti del reticolo, 
				calcolando le interazioni con i siti vicini nelle quattro direzioni spazio-temporali. 
				L'accesso sequenziale \`e reso particolarmente difficile dalla natura quadridimensionale
				del problema. Questo forza il processore ad accedere ad aree di memoria non contigue e 
				distanti fra loro, forzando un'accesso continuo alla RAM e degradando le prestazioni. 
				Questo ci spinge a cercare una tecnica per istruire la CPU a richiedere i dati
				\emph{qualche ciclo di clock} prima che essi siano realmente utilizzati.
				
				Per ovviare a questo, si introducono nel codice delle istruzioni particolari (di 
				\emph{prefetching})	che indicano alla CPU quali informazioni caricare in anticipo, in 
				maniera tale da evitare il problema appena esposto.
				
		\section{La metodologia d'analisi}		
			Per valutare il progresso nelle prestazioni, da un lato utilizziamo un insieme di metriche 
			standard descritte di seguito, dall'altro il pacchetto tmLQCD comprende un programma 
			pensato per studiare la velocit\`a di esecuzione dell'operatore di Dirac, descritto nella
			sezione sucessiva.
			
			Le informazioni congiunte ottenute da queste due fonti ci hanno suggerito 
			quali modifiche erano interessanti, dove andavano eseguite e in che maniera 
			implementate.
				
			\subsection{Metriche utilizzate}
				L'analisi del software \`e stata effettuata analizzando un piccolo insieme di metriche 
				(dinamiche e statiche) ritenute rilevanti per la comprensione di eventuali 
				colli di bottiglia. 
			
				Per \emph{metriche dinamiche} intendiamo le informazioni che sono strettamente 
				dipendenti dall'esecuzione del programma. In questo insieme, abbiamo considerato:
			
				%
				%	TODO: aggiungere altre informazioni
				%
					\begin{center}
						\begin{itemize}
							\item{tempo di esecuzione effettivo (``walltime'')}
							\item{cache misses}
						\end{itemize}
					\end{center}
					
				Per raccogliere queste informazioni abbiamo utilizzato Perf \cite{perf-homepage}.
			
				Mentre per \emph{metriche statiche} intendiamo le informazioni legate all'esecuzione 
				del codice a livello piu' basso possibile e strettamente connesse alla microarchitettura
				del processore. Per queste informazioni abbiamo considerato le statistiche ottenute 
				dal IACA \cite{IACA-homepage} (Intel Architecture Code Analyzer). 
				Questo programma analizza il codice staticamente, fornendo delle informazioni 
				sull'utilizzo delle varie porte d'esecuzione del processore ed evidenziando 
				eventuali colli di bottiglia dovuti all'utilizzo di particolari risorse della cpu.
				
				I dati forniti da quest'ultima utility sono particolarmente preziosi, in quanto 
				permettono di capire quali sequenze di istruzioni, a parit\`a di problema, possono 
				fornire le migliori prestazioni: infatti due programmi scritti in maniera tale da 
				eseguire le stesse operazioni ma formati da \emph{diverse sequenze} d'istruzioni 
				assembler possono avere velocit\`a d'esecuzione molto differenti.
				
				Questo strumento ci ha permesso di scartare alternative che, da un punto di vista 
				prestazionale, non rappresentavano miglioramenti. 
				
				%
				%	TODO: aggiungere il fatto che abbiamo analizzato tsplit e halfspinor
				%
				
			\subsection{Il benchmark di tmLQCD}

				Il programma di benchmark fornisce delle informazioni riguardanti 
				la velocit\`a di esecuzione dell'operatore di Dirac: in particolare
				calcola il numero di volte che pu\`o essere applicato ad un reticolo di dimensione
				fissata. 
				
				Avendo una stima del costo computazione della singola applicazione in termini 
				di operazioni al secondo, \`e possibile ricavare quante operazioni in virgola 
				mobile il programma riesce ad eseguire.
				
				La formula utilizzata \`e la seguente:
				
					$$ 1608 \cdot \left[\frac{\text{flop}}{\text{sito}}\right] \cdot \frac{\text{numero siti visitati}}{\text{tempo esecuzione}} \left[\frac{\text{sito}}{\text{s}}\right] =  \text{flops} \left[ \frac{\text{flop}}{\text{s}}\right] $$

				Il coefficiente iniziale rappresenta il costo in termini di operazioni in virgola mobile 
				necessarie per effettuare un'unica applicazione dell'operatore di Dirac.
				
				\`E da notare come quest'ultima metrica non aggiunga altre informazioni
				gi\`a presenti nelle informazioni raccolte con i programmi di analisi 
				citati in precedenza. Questa informazione serve piuttosto come verifica 
				a posteriori delle nostre modifiche.
				

			Tutti i test sono stati eseguiti su un sistema quadcore uniprocessore: quest'ultima 
			scelta deriva dall'impossibilit\`a di replicare i test per la versione
			modificata del programma in sistemi distribuiti. Tale scelta non dovrebbe influenzare 
			concettualmente i risultati ottenuti, in quanti i tempi caratteristici considerati sono 
			molto differenti tra loro.
			
		\section{La struttura del codice}
			La funzione che rappresenta l'operatore di Dirac \`e espressa in termini di un piccolo insieme 
			di procedure elementari, indipendenti tra loro e che utilizzano un ristretto numero di registri 
			per eseguire l'input e l'output.
			
			L'indipendenza reciproca di queste funzioni ci permette di considerare ogni 
			singola routine come dei \emph{building blocks} dell'operatore nella sua globalit\`a. 
			Questo significa che, lasciando invariati i registri usati per effettuare l'input e 
			l'output, possiamo modificare il comportamento delle suddette sottoprocedure 
			sapendo che il comportamento della routine dell'operatore di Dirac fornir\`a 
			sempre lo stesso risultato numerico. 
			
			Fatta questa osservazione, il problema della riscrittura dell'operatore di Dirac 
			si riduce alla conversione delle singole sottoprocedure che lo compongono.
			
			Ognuna di queste routine elementari esegue un compito piuttosto semplice 
			e ben circoscritto. Elenchiamo di seguito quelle pi\`u importanti: il codice 
			seguente \`e relativo alla versione originale (reperibile online \cite{tmLQCD-su3-source},
			come quello relativo alla versione che supporta AVX \cite{tmLQCD-avx-source}):
			
				\begin{center}
					\begin{tabular}{ c }
						\hline 
						\_sse\_su3\_multiply \\
						\_sse\_su3\_inverse\_multiply \\
						\_sse\_vector\_cmplx\_mul \\
						\_sse\_vector\_cmplxcg\_mul \\
						\_sse\_vector\_i\_mul \\
						\hline 
					\end{tabular}
				\end{center}											
			
			Di seguito forniamo una breve descrizione degli algoritmi implementati dalle 
			varie procedure. 
			
			\subsection{\_sse\_su3\_multiply}
				Questa funzione implementa la moltiplicazione di una componente dello spinore
				per la matrice di gauge $SU(3)$:
				
					$$
						\begin{pmatrix}
							U_{0, 0} & U_{0, 1} & U_{0, 2} \\
							U_{1, 0} & U_{1, 1} & U_{1, 2} \\			
							U_{2, 0} & U_{2, 1} & U_{2, 2}
						\end{pmatrix} \cdot 
						\begin{pmatrix}
							c_0 \\
							c_1 \\
							c_2 
						\end{pmatrix}
					$$
				
				
			\subsection{\_sse\_su3\_inverse\_multiply} 
				Questa funzione implementa la moltiplicazione di una componente dello spinore 
				per il coniugato di $SU(3)$.
				
					$$
						\begin{pmatrix}
							U_{0, 0} & U_{0, 1} & U_{0, 2} \\
							U_{1, 0} & U_{1, 1} & U_{1, 2} \\			
							U_{2, 0} & U_{2, 1} & U_{2, 2}
						\end{pmatrix}^{\dagger} \cdot 
						\begin{pmatrix}
							c_0 \\
							c_1 \\
							c_2 
						\end{pmatrix}
					$$
				
			\subsection{\_sse\_vector\_cmplx\_mul}
				Moltiplica una componente dello spinore per un numero complesso $c$
				
					$$
						\begin{pmatrix}
							c_0 \\
							c_1 \\
							c_2 
						\end{pmatrix} \cdot c
					$$
				
			\subsection{\_sse\_vector\_cmplxcg\_mul}
				Moltiplica una componente dello spinore per il coniugato di un numero 
				complesso $c$:
				
					$$
						\begin{pmatrix}
							c_0 \\
							c_1 \\
							c_2 
						\end{pmatrix} \cdot {c}^{\ast}
					$$
				
			\subsection{\_sse\_vector\_i\_mul} 
				Moltiplica una componente dello spinore per $i$:				

					$$
						\begin{pmatrix}
							c_0 \\
							c_1 \\
							c_2 
						\end{pmatrix} \cdot i
					$$

			
\end{document}
