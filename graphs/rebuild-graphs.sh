#!/bin/sh
for i in ./graph-*.plt
do
	# output info
	echo "Processing $i .."

	# run gnuplot
	gnuplot $i
	if [ $? -ne 0 ]
	then
		echo "  " $i " failed."
		exit
	fi

	# run gedit
	latex `echo $i | sed 's/plt/tex/'`
	dvipdf `echo $i | sed 's/plt/dvi/'`

	# rebuild image
#	epstopdf `echo $i | sed 's/plt/eps/'`
done
