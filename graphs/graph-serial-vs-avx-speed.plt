# terminal setup: we want eps output
#set terminal postscript eps size 3.5,2.62 enhanced color \
#    font 'Helvetica,15' linewidth 2

# terminal setup: we want eps output
set terminal epslatex standalone color colortext linewidth 2

set output "graph-serial-vs-avx-speed.tex"

# labels 
set format "$%g$"
set xlabel "Configurazione"
set ylabel "MFlops"

# title
set title "Comparazione versione con e senza modifiche" 

# actual data
plot "avx.dat" u 1:2:xticlabel(4) t "con modifiche" w lines ls 1 lc 1, \
	 "serial.dat" u 1:2:xticlabel(4) t "senza modifiche" w lines ls 1 lc 2

unset output
exit
