# terminal setup: we want eps output
#set terminal postscript eps size 3.5,2.62 enhanced color \
#    font 'Helvetica,15' linewidth 2
    
# terminal setup: we want eps output
set terminal epslatex standalone color colortext linewidth 2

set output "graph-serial-vs-slowest-speed.tex"

# labels 
set format "$%g$"
set xlabel "Configurazione"
set ylabel "MFlops"

# title
set title "Confronto versione accelerata e non"

# actual data
plot "slowest.dat" u 1:2:xticlabel(4) t "versione non accelerata" w lines ls 1 lc 1, \
	 "serial.dat" u 1:2:xticlabel(4) t "versione accelerata" w lines ls 1 lc 2

exit
