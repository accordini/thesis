# terminal setup: we want eps output
#set terminal postscript eps size 3.5,2.62 enhanced color \
#    font 'Helvetica,15' linewidth 2

# terminal setup: we want eps output
set terminal epslatex standalone color colortext linewidth 2

set output "graph-serial-speed.tex"

# labels 
set format "$%g$"
set xlabel "Configurazione"
set ylabel "MFlops"

# title
set title "Versione seriale"

# actual data
plot "serial.dat" u 1:2:xticlabel(4) notitle w lines ls 1

unset output
exit
