#set terminal postscript eps size 3.5,2.62 enhanced color \
#    font 'Helvetica,15' linewidth 2

# terminal setup: we want eps output
set terminal epslatex standalone color colortext linewidth 2

set output "graph-mpi-ts-vs-mpi-avx-ts-speed.tex"

# labels 
set format "$%g$"
set xlabel "Configurazione"
set ylabel "MFlops"

# title
set title "Comparazione versione MPI tsplit con e senza modifiche" 

# data ranges
set yrange[2800:4000]

# actual data
plot "mpi-avx-ts.dat" u 1:2:xticlabel(4) t "con modifiche" w lines ls 1 lc 1, \
	 "mpi-ts.dat" u 1:2:xticlabel(4) t "senza modifiche" w lines ls 1 lc 2

# close it
unset output
exit
