\documentclass[main.tex]{subfiles}
\begin{document}
	\chapter{Risultati e conclusioni}			
		\section{La versione non modificata}
			Mostriamo di seguito il grafico relativo alle prestazioni di calcolo della versione 
			seriale del programma accelerato SSE, senza le modifiche apportate relative al supporto AVX. 
			Ogni grafico mostra i dati mediati su 10 esecuzioni del programma:
			
				\begin{center}
					\includegraphics{graphs/graph-serial-speed}
				\end{center}
						
			Questi dati mostrano un picco iniziale, dovuto al fatto che il problema in esame
			possiede una dimensione sufficientemente piccola per risiedere per tutto il tempo 
			d'esecuzione del programma negli strati pi\`u elevati della cache del sistema. 
			
			In seguito, le prestazioni diventano pressoch\'e costanti, segno che la velocit\`a 
			d'esecuzione del programma \`e limitata dalla velocit\`a d'accesso alla memoria. 
			
			La versione pensata per sfruttare MPI mostra un comportamente meno triviale:
			
				\begin{center}
					\includegraphics{graphs/graph-serial-mpi-speed}
				\end{center}
			
			I dati sono relativi alle prestazioni su ogni core: la velocit\`a d'esecuzione 
			dell'operatore su singolo core, come \`e lecito aspettarsi,\`e nettamente pi\`u 
			bassa: questo \`e causato dall'overhead delle primitive MPI, ma anche dall'aver
			disabilitato il codice relativo al prefetching.
			
		\section{La versione modificata}
			Sono state sviluppate pi\`u versioni del codice, con lo scopo di verificare il miglior
			guadagno prestazionale raggiungibile.
			
			Riportiamo in seguito i risultati della versione AVX pi\`u rapida ottenuta, messa a 
			confronto con la controparte SSE:
			
				\begin{center}
					\includegraphics{graphs/graph-serial-vs-avx-speed}
				\end{center}
				
			E i risultati relativi alla versione MPI:
			
				\begin{center}
					\includegraphics{graphs/graph-mpi-ts-vs-mpi-avx-ts-speed}
				\end{center}			
			
		\section{I problemi riscontrati}

			\`E evidente che il codice deve essere ancora messo a punto per poter competere 
			con l'implementazione originale. Ma ci sono alcuni fattori che vanno messi in 
			evidenza per poter fornire una spiegazione soddisfacente del tentativo infruttuoso
			mostrato in questo lavoro. 
			
			Un punto da tenere in considerazione \`e rappresentato dal fatto che le 
			istruzioni AVX \emph{non sono complete}. L'implementazione del set d'istruzioni 
			presente nei processori attualmente disponibili sul mercato fornisce solamente 
			un sottoinsieme delle funzionalit\`a dichiarate da Intel. Il set completo verr\`a 
			introdotto con l'architettura \emph{Haswell}, sotto il nome AVX2. Nelle istruzioni 
			non disponibili vi \`e una serie di primitive denominate \emph{FMA}: esse permettono di 
			esprimere espressioni come:
			
				$$ a = a + b \cdot c $$
				
			utilizzando un'unica istruzione AVX. In effetti, la possibilit\`a di utilizzare questa
			funzionalit\`a era uno dei punti principali dietro alle motivazioni di questo lavoro. 
			
			Un commento va dedicato anche a come le informazioni sono immagazzinate in memoria: 
			le strutture dati utilizzate nel software (in particolare quelle relative alla definizione 
			dello spinore e della matrice $SU(3)$) semplicemente non sono ottimali all'approccio 
			tentato in questo lavoro. Le istruzioni AVX (ma anche la controparte SSE) si aspettano 
			informazioni immagazzinate in \emph{strutture di array} e non \emph{array di strutture}. 
			La differenza \`e fondamentale, in quanto le istruzioni SIMD sono progettate per 
			avere in input dati in questa forma.
			
			Infine, la scrittura di codice a un livello cos\`i basso comporta delle problematiche
			non indifferenti: \`e necessaria una profonda conoscienza dell'architettura sulla 
			quale il codice viene eseguito, in particolare quali risorse vengono utilizzate dalle 
			varie istruzioni e quali combinazioni di quest'ultime \`e consigliabile evitare. 
			
		\section{Possibili soluzioni}
			Sono state elaborate alcune possibili soluzioni per i problemi riscontrati che,
			per\`o richiederebbero una quantit\`a di tempo troppo elevata rispetto agli obbiettivi
			di questo lavoro.
			
			\subsection{Calcolo parallelo delle componenti}
				Il codice relativo alla parte non diagonale dell'operatore di Dirac (\emph{Hopping Matrix})	
				\`e stato suddiviso in piccole porzioni, ognuna delle quali calcola l'aggiornamento in 
				entrambe le direzioni (avanti e indietro) lungo una dimensione spazio-temporale. In particolare, 
				la cosa che accomuna queste operazioni, \`e l'esecuzione \emph{doppia} delle operazioni
				riguardanti le moltiplicazioni tra spinori e matrici di gauge. 
				
				Questo punto non deriva dall'effettiva necessit\`a matematica di effettuare due 
				volte tale operazione, ma piuttosto dal fatto che le istruzioni SSE, 
				avendo registri di dimensione pari a 128 bit, permettono di gestire
				un solo numero complesso in doppia precisione alla volta. 
				
				La nuova dimensione dei registri potrebbe permetterebbe di riutilizzare
				gran parte del il codice esiste, lasciando l'algoritmo sottostante pressoch\'e invariato
				ma estendolo in maniera tale da dimezzare il numero di moltiplicazioni
				matrice-vettore eseguite.
				
				Questa strada per\`o comporta una modifica sostanziale di come l'operatore
				viene calcolato, e quindi esula dagli obbiettivi di questo lavoro. 
				
			\subsection{Modifiche al layout della memoria}
				Come gi\`a specificato sopra, la struttura dei dati in memoria non \`e 
				``SIMD-friendly''. Si potrebbe quindi modificare il codice in maniera tale 
				da renderla pi\`u adatta alle istruzioni utilizzate.  
				
				L'idea \`e quella utilizzata in \cite{Smelyanskiy:2011:HLQ:2063384.2063477}: per sfruttare al massimo
				il parallelismo fornito dalle istruzioni AVX i dati degli spinori e dei campi
				di gauge vengono memorizzati non come \emph{array di strutture}, ma bens\`i come
				\emph{strutture di array}.
				
				In questo caso per\`o la questione \`e chiaramente pi\`u delicata, in quanto 
				la modifica di una struttura dati cos\`i importante per il programma provocherebbe 
				una quantit\`a significativa di cambiamenti al codice.
			
			\subsection{Prefetching}
				Nelle versioni modificate e quelle parallelizzate lungo la dimensione temporale, 
				il prefetching \`e disabilitato: le modifiche introdotte da quest'ultima modalit\`a 
				di calcolo sono incompatibili con il codice preesistente. 
				
				\`E lecito aspettarsi che la reintroduzione di una tecnica di prefetch compatibile 
				con la versione in esame introduca sostanziali incrementi nelle prestazioni.				
								
		\section{Conclusioni}
			Questo lavoro rappresenta un primo passo verso la conversione del codice esistente
			in una chiave pi\`u moderna rispetto alle tecnologie presenti sul mercato.
			
			I risultati, come gi\`a osservato non hanno portato il guadagno prestazionale sperato
			all'inizio. Nonostante questo, altri lavori \cite{Smelyanskiy:2011:HLQ:2063384.2063477} suggeriscono che la strada
			sia difficile ma fruttuosa.
			
			Questo lavoro inoltre pu\`o rappresentare la base di sviluppo per rispondere ad alcune domande
			sollevate dal tema di AuroraScience \cite{2012slft.confE.192D}, riguardanti i guadagni prestazionali
			relativi all'utilizzo in campo scientifico delle istruzioni AVX.
			
			Le modifiche apportate in questo lavoro sono disponibili sottoforma di repository github \cite{tmlqcd-avx-github}.

\end{document}
